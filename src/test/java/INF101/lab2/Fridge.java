package INF101.lab2;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    int fridgeCapasaty = 20;
    int nItemsInFridge = 0;
    List<FridgeItem> ItemsInFridge = new ArrayList<FridgeItem>();


    
    @Override
    public int nItemsInFridge() {
        
        return (nItemsInFridge);
    }

    
    @Override
    public int totalSize() {
        return (fridgeCapasaty);
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge < fridgeCapasaty) {
            
            this.ItemsInFridge.add(item);
            nItemsInFridge = ItemsInFridge.size();
            return true;
        }
        
        else {
        return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!this.ItemsInFridge.contains(item)) {
            throw new NoSuchElementException("Item not in fridge");
        }
        else {
        this.ItemsInFridge.remove(item);
        nItemsInFridge = ItemsInFridge.size();
        } 
    }

    @Override
    public void emptyFridge() {
        ItemsInFridge.clear();
        nItemsInFridge = ItemsInFridge.size();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List <FridgeItem> expiredItems = new ArrayList<FridgeItem>();

        int i = 0;
        while (i < ItemsInFridge.size()) {
            FridgeItem Item = ItemsInFridge.get(i);
            
            if (Item.hasExpired()) {
                expiredItems.add(Item);
                
                nItemsInFridge -= 1;
            }
            i += 1;
        }
        ItemsInFridge.removeAll(expiredItems);
        
        return (expiredItems);
    }

}
